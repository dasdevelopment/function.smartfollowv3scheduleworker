using Das.Oltp;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SmartFollowV3ScheduleWorker.Models;
using SmartFollowV3ScheduleWorker.Routines;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartFollowV3ScheduleWorker
{
    public class SmartFollowV3ScheduleWorker
    {
        [FunctionName("smart-follow-v3-schedule")]
        public async Task SmartFollowSchedule([QueueTrigger("%SmartFollowScheduleQueueName%", Connection = "AzureStorageConnString")] string message, ILogger log)
        {
            log.LogInformation($"Processing message: {message}");
            
            var queueMessage = JsonConvert.DeserializeObject<ScheduleQueueMessage>(message);
            var franchiseId = queueMessage.franchise_id;

            
            var franchiseScheduleTask = OltpFunctions.GetFranchiseSmartFollowScheduleDaysAsync(franchiseId);
            var franchiseSchedule = await franchiseScheduleTask;

            //Only start work if dealer has 3.0 schedule.
            if (franchiseSchedule.Count > 0)
            {
                
                var apiFranchisesTask = OltpFunctions.GetAPICRMFranchiseIdsAsync();
                var apiFranchises = await apiFranchisesTask;

                var franchiseTask = OltpFunctions.GetFranchiseSettings(franchiseId);
                var franchise = await franchiseTask;

                int smartFollowNotificationPreempt = 1; // 1 day for now, customizable later
                var smartFollowDays = franchiseSchedule
                    .Select(schedule => schedule.day)
                    .ToList();
                var notificationDay = smartFollowDays.Min() - 1;

                // Artifically adding the notificationDay to the schedule so that we could SF activation Notification 
                smartFollowDays.Add(notificationDay);

                var leads = await OltpFunctions.GetLeadsAsync(franchiseId, smartFollowDays);


                

                foreach (var lead in leads)
                {
                    var daysSinceLeadReceived = (int)Math.Ceiling((DateTime.Today - lead.created_date).TotalDays);

                    if (daysSinceLeadReceived == notificationDay && franchise.send_smart_follow_activation_notification == true)
                    {
                        var scheduleDetails = franchiseSchedule
                            .Where(schedule => schedule.day == daysSinceLeadReceived + smartFollowNotificationPreempt)
                            .FirstOrDefault();

                        if (scheduleDetails == default || (lead.type.ToUpper() == "NEW" && scheduleDetails.new_active == false) || (lead.type.ToUpper() == "USED" && scheduleDetails.used_active == false))
                            continue;

                        await NotificationRoutine.ScheduleAsync(lead.lead_id, daysSinceLeadReceived + smartFollowNotificationPreempt);

                        log.LogInformation($"SmartFollow notification scheduled for lead ID {lead.lead_id} (FID: {lead.franchise_id}): Created date is {lead.created_date} and notification is for SmartFollow day {(int)(DateTime.Today - lead.created_date).TotalDays + smartFollowNotificationPreempt} ");
                    }

                    else if (smartFollowDays.Contains(daysSinceLeadReceived))
                    {
                        var scheduleDetails = franchiseSchedule
                            .Where(schedule => schedule.day == daysSinceLeadReceived)
                            .FirstOrDefault();

                        if (scheduleDetails == default || (lead.type.ToUpper() == "NEW" && scheduleDetails.new_active ==false) || (lead.type.ToUpper() == "USED" && scheduleDetails.used_active == false))
                            continue;

                        if (apiFranchises.Contains(franchiseId) && !string.IsNullOrEmpty(lead.lp_customer_number) && !string.IsNullOrEmpty(lead.lp_lead_number) && !string.IsNullOrEmpty(lead.crm_reference))
                            await SmartFollowRoutine.ScheduleAPIAsync(lead, scheduleDetails, franchise.send_smart_follow_activation_notification);

                        else
                            await SmartFollowRoutine.ScheduleAsync(lead, scheduleDetails, franchise.send_smart_follow_email_notification);

                        log.LogInformation($"SmartFollow scheduled for lead ID {lead.lead_id}: Created date is {lead.created_date} and SmartFollow day is {(int)(DateTime.Today - lead.created_date).TotalDays}.");
                    }
                }
            }
        }
    }
}
