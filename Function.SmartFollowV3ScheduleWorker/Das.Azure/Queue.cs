﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Threading.Tasks;

namespace Das.Azure
{
    public static class Queue
    {
        public static async Task SendMessageAsync(string connectionString, string queueName, string msg)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
                CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
                CloudQueue client = queueClient.GetQueueReference(queueName);
                await client.CreateIfNotExistsAsync();
                CloudQueueMessage message = new CloudQueueMessage(msg);
                await client.AddMessageAsync(message);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error sending msg to {0}, EX: {1}, MSG: {2} ", queueName, ex.ToString(), msg));
            }
        }
    }
}
