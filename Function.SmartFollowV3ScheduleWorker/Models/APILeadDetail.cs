﻿namespace SmartFollowV3ScheduleWorker.Models
{
    public class APILeadDetail
    {
        public string CRMLeadId { get; set; } = string.Empty;
        public string CRMCustomerId { get; set; } = string.Empty;
        public string CRMReference { get; set; } = string.Empty;
        public LeadSchedule RLLeadScheduleDetail { get; set; }
    }
}
