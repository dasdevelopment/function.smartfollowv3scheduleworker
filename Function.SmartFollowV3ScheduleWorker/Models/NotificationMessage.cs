﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartFollowV3ScheduleWorker.Models
{
    public class NotificationMessage
    {
        public Guid lead_id { get; set; }
        public int day { get; set; }
    }
}
