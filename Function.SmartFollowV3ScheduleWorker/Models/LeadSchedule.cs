﻿using System;

namespace SmartFollowV3ScheduleWorker.Models
{
    public class LeadSchedule
    {
        public Guid lead_id { get; set; }
        public string type { get; set; }
        public Guid? template_values_id { get; set; }
        public int day { get; set; }
        public bool send_smart_follow_email_notification { get; set; }
    }
}
