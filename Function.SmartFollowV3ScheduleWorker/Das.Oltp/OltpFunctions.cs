using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Das.Oltp
{
    public class OltpFunctions
    {
        public static async Task<List<Franchise_Smart_Follow_v3_Schedule>> GetFranchiseSmartFollowScheduleDaysAsync(int franchise_id)
        {
            try
            {
                using (var ctx = new OltpContext())
                {
                    var rawQuery = @"
SELECT
	fsfvs.franchise_id,
	fsfvs.day,
	cast(case when fsfvcs.smart_follow_new = 0 THEN 0 ELSE fsfvs.new_active END AS bit) as 'new_active',
	fsfvs.new_template_values_id,
	cast(case when fsfvcs.smart_follow_used = 0 THEN 0 ELSE fsfvs.used_active END AS bit) as 'used_active',
	fsfvs.used_template_values_id
FROM
	franchise_smart_follow_v3_schedule fsfvs with(nolock)
JOIN
	smart_follow_franchise sff with(nolock) on fsfvs.franchise_id = sff.franchise_id
JOIN
	franchise_smart_follow_v3_campaign_settings fsfvcs with (nolock) on fsfvcs.franchise_id= sff.franchise_id
WHERE
	sff.status = 'Active SmartFollow 3.0'
AND((fsfvcs.smart_follow_new = 1 AND fsfvs.new_active = 1) or (fsfvcs.smart_follow_used = 1 AND fsfvs.used_active = 1))
AND fsfvs.franchise_id = {0}
";

                    string query = string.Format(rawQuery, franchise_id);
                    var list = await ctx.franchise_smart_follow_v3_schedule.FromSqlRaw(query).ToListAsync();

                    return list;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Getting SmartFollow Schedule : " + ex.ToString());
            }
        }

        public static async Task<List<Lead>> GetLeadsAsync(int franchise_id, List<int> schedule)
        {
            try
            {
                using (var ctx = new OltpContext())
                {
                    ctx.Database.SetCommandTimeout(300);
                    var query = @"
SELECT
	l.lead_id,
    l.franchise_id,
    l.type,
	l.created_date,
    l.lp_customer_number ,
    l.lp_lead_number,
    l.crm_reference
FROM
	lead l with (nolock)
JOIN
	franchise_consumer_txn fct with (nolock) on (l.lead_id = fct.txn_id)
JOIN
	franchise_consumer fc with (nolock) on (fct.franchise_consumer_id = fc.franchise_consumer_id
	AND isnull(fc.pause_smart_follow_until_date, getdate()-1) < getdate()
	AND subscribed_to_all_campaigns = 1
	AND fc.smart_follow_txn_id = l.lead_id)
WHERE
    l.franchise_id = @franchise_id
	AND l.status in ('Campaigning SmartFollow', 'Inactive Off Hours SmartFollow')
	AND (l.lp_make !='unknown' and lp_make is not null and l.lp_make !='' and lp_model !='unknown')
    AND (
";
                    string createdDateParameter = string.Empty;
                    for (int i = 0; i< schedule.Count(); i++)
                    {
                        var dateStart = DateTime.Today.AddDays(-schedule[i]).ToString("yyyyMMdd");
                        var dateEnd = DateTime.Today.AddDays(-schedule[i] + 1).ToString("yyyyMMdd");
                        createdDateParameter += @$"l.created_date between '{dateStart}' and '{dateEnd}' ";
                        if (i != schedule.Count()-1)
                            createdDateParameter += " or ";
                    }
                    createdDateParameter += ")";
                    query += createdDateParameter;
                    var franchiseIdParam = new SqlParameter("franchise_id", franchise_id);
                    var list = await ctx.lead
                        .FromSqlRaw(query, franchiseIdParam)
                        .ToListAsync();
                    
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Getting sf lead : " + ex.ToString());
            }
        }

        public static async Task<List<int>> GetAPICRMFranchiseIdsAsync()
        {
            try
            {
                using (var ctx = new OltpContext())
                {
                    ctx.Database.SetCommandTimeout(300);
                    var query = @"
SELECT
    fi.franchise_id
FROM
    franchise_identifier fi with(nolock)
JOIN
    smart_follow_franchise sff with(nolock) on fi.franchise_id = sff.franchise_id
JOIN
    franchise_v f with(Nolock) on f.franchise_id = fi.franchise_id
WHERE
    identifier like '%apipost.http.responselogix.com'
    AND customer_name not like '%test%'
";

                    var list = await ctx.ApiFranchises.FromSqlRaw(query).ToListAsync();

                    return list.Select(franchise => franchise.franchise_id).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting Api Franchise: " + ex.ToString());
            }
        }

        public static async Task<Franchise> GetFranchiseSettings(int franchiseId)
        {
            try
            {
                using (var ctx = new OltpContext())
                {
                    ctx.Database.SetCommandTimeout(300);
                    var rawQuery = @"
SELECT
    f.franchise_id,
    f.send_smart_follow_activation_notification,
    f.send_smart_follow_email_notification
FROM
    franchise f with(nolock)
WHERE
    f.franchise_id = {0}
";

                    var query = string.Format(rawQuery, franchiseId);
                    var franchise = await ctx.franchise.FromSqlRaw(query).FirstOrDefaultAsync();

                    return franchise;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting SmartFollow lead: " + ex.ToString());
            }
        }
    }

}
