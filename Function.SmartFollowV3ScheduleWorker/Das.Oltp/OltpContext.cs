﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace Das.Oltp
{

    public class Franchise_Smart_Follow_v3_Schedule
    {
        public int franchise_id { get; set; }
        public int day { get; set; }
        public bool new_active { get; set; }
        public Guid? new_template_values_id { get; set; }
        public bool used_active { get; set; }
        public Guid? used_template_values_id { get; set; }

    }

    public class Lead
    {
        [Key]
        public Guid lead_id { get; set; }
        public int franchise_id { get; set; }
        public string type { get; set; }
        public DateTime created_date { get; set; }
        public string lp_customer_number { get; set; }
        public string lp_lead_number { get; set; }
        public string crm_reference { get; set; }
    }

    public class ApiFranchise
    {
        [Key]
        public int franchise_id { get; set; }
    }
    
    public class Franchise
    {
        [Key]
        public int franchise_id { get; set; }
        public bool send_smart_follow_activation_notification { get; set; }
        public bool send_smart_follow_email_notification { get; set; }
    }



    public class OltpContext : DbContext
    {
        public DbSet<Franchise_Smart_Follow_v3_Schedule> franchise_smart_follow_v3_schedule { get; set; }
        public DbSet<Lead> lead { get; set; }
        public DbSet<Franchise> franchise { get; set; }
        public DbSet<ApiFranchise> ApiFranchises { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise_Smart_Follow_v3_Schedule>()
                .HasKey(c => new { c.franchise_id, c.day });
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            optionsBuilder.UseSqlServer(Environment.GetEnvironmentVariable("OltpConnString"));
        }

    }

}