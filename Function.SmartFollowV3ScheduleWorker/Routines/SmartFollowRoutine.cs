﻿using Das.Oltp;
using Newtonsoft.Json;
using SmartFollowV3ScheduleWorker.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartFollowV3ScheduleWorker.Routines
{
    public static class SmartFollowRoutine
    {
        public static async Task ScheduleAsync(Lead lead, Franchise_Smart_Follow_v3_Schedule schedule, bool send_sf_email_notification = false)
        {
            var leadSchedule = new LeadSchedule()
            {
                lead_id = lead.lead_id,
                day = schedule.day,
                template_values_id = lead.type.ToLower() == "new" ? schedule.new_template_values_id : schedule.used_template_values_id,
                type = lead.type,
                send_smart_follow_email_notification = send_sf_email_notification

            };

            await Das.Azure.Queue.SendMessageAsync(
                Environment.GetEnvironmentVariable("AzureStorageConnString"),
                Environment.GetEnvironmentVariable("SmartFollowPrepareLeadQueueName"),
                JsonConvert.SerializeObject(leadSchedule));
        }

        public static async Task ScheduleAPIAsync(Lead lead, Franchise_Smart_Follow_v3_Schedule schedule, bool send_sf_email_notification = false)
        {
            var leadSchedule = new LeadSchedule()
            {
                lead_id = lead.lead_id,
                day = schedule.day,
                template_values_id = lead.type.ToLower() == "new" ? schedule.new_template_values_id : schedule.used_template_values_id,
                type = lead.type,
                send_smart_follow_email_notification = send_sf_email_notification
            };

            var apiLeadDetail = new APILeadDetail()
            {
                RLLeadScheduleDetail = leadSchedule,
                CRMCustomerId = lead.lp_customer_number,
                CRMLeadId = lead.lp_lead_number,
                CRMReference = lead.crm_reference
            };

            await Das.Azure.Queue.SendMessageAsync(
                Environment.GetEnvironmentVariable("StorageRLConnString"),
                Environment.GetEnvironmentVariable("SmartFollowApiPrepareLeadQueueName"),
                JsonConvert.SerializeObject(apiLeadDetail));
        }
    }
}
