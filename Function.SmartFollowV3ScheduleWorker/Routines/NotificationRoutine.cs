﻿using Das.Oltp;
using Newtonsoft.Json;
using SmartFollowV3ScheduleWorker.Models;
using System;
using System.Threading.Tasks;

namespace SmartFollowV3ScheduleWorker.Routines
{
    public static class NotificationRoutine
    {
        public static async Task ScheduleAsync(Guid lead_id, int day)
        {
            var notificationMessage = new NotificationMessage()
            {
                lead_id = lead_id,
                day = day
            };

            await Das.Azure.Queue.SendMessageAsync(
                Environment.GetEnvironmentVariable("AzureStorageConnString"),
                Environment.GetEnvironmentVariable("SmartFollowNotificationQueueName"),
                JsonConvert.SerializeObject(notificationMessage));
        }
    }
}
